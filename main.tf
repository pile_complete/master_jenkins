#créer  un resource group
resource "azurerm_resource_group" "rg" {
    name = "${var.name}"
    location = "${var.location}"

    tags {
        owner = "${var.owner}"
    }
}
#créer un virtual network
resource "azurerm_virtual_network" "myFirstVnet" {
    name = "${var.name_vnet}"
    address_space = "${var.adress_space}"
    location = "${var.location}"
    resource_group_name = "${azurerm_resource_group.rg.name}"
}
#créer un Subnet
resource "azurerm_subnet" "myFirstSubnet" {
    name = "${var.name_subnet}"
    resource_group_name = "${azurerm_resource_group.rg.name}"
    virtual_network_name = "${azurerm_virtual_network.myFirstVnet.name}"
    address_prefix = "${var.address_prefix}"
}
#créer une public IP
resource "azurerm_public_ip" "publicIP" {
  name                = "${var.name_IP}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  allocation_method   = "${var.alloc_method}"
  domain_name_label   = "tp-pile"
}
#créer un NSG
resource "azurerm_network_security_group" "mySecurityGroup" {
  name                = "${var.name_Security_Group}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Jenkins"
    priority                   = 1003
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "azurerm_network_interface" "myNetworkInterface" {
  name                = "${var.nwi_name}"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  network_security_group_id = "${azurerm_network_security_group.mySecurityGroup.id}"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = "${azurerm_subnet.myFirstSubnet.id}"
    private_ip_address_allocation = "${var.alloc_method}"
    public_ip_address_id = "${azurerm_public_ip.publicIP.id}"
  }
}
resource "azurerm_storage_account" "myStorage" {
  name                     = "grosgibsa"
  resource_group_name      = "${azurerm_resource_group.rg.name}"
  location                 = "${var.location}"
  account_tier             = "Standard"
  account_replication_type = "GRS"
}
resource "azurerm_virtual_machine" "main" {
  name                  = "${var.vm_name}"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.rg.name}"
  network_interface_ids = ["${azurerm_network_interface.myNetworkInterface.id}"]
  vm_size               = "${var.vm_size}"

  storage_image_reference {
    publisher = "Openlogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  storage_os_disk {
    name              = "mydisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "grosgib"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }
  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
        path="/home/stage/.ssh/authorized_keys"
        key_data = "${var.key_data}"
    }
  }
}